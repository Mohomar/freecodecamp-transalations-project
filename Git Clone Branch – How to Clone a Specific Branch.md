# Git Clone Branch – How to Clone a Specific Branch

![Bolaji Ayodeji](https://www.freecodecamp.org/news/content/images/size/w100/2020/10/IMG_6991.jpeg)

[Bolaji Ayodeji](https://www.freecodecamp.org/news/author/bolajiayodeji/)

![Git Clone Branch – How to Clone a Specific Branch](https://www.freecodecamp.org/news/content/images/size/w2000/2020/06/article-banner--1-.gif)

بخلاف برامج إدارة الاصدارات (version control systems) مثل SVN و CVS ، برنامج Git موزع. اي مطور له تاريخ كامل و تحكم على الكود الخاص به على جهازه المحلي او على عن بعد. بإمكانهم الوصول و التعامل مع اجزاؤ الكود المختلفة حسبما يتراءى لهم من اماكن تواجدهم المختلفة.

منذ أن أنشأ Linus Torvalds  ( المنشأ الشهير  لنواة  نظام التشغيل لينكس ) Git فى عام 2005 لتطوير نواة اللينكس . اصبح من اكثر نظم ادارة الاصدارات الحديثة انتشارا فى العالم.

فى هذه المقال، سوف اعرض لك مقدمة عن  Git clone و Git branch workflows ، و سوف اعرض لك كيف تقوم بإستنساخ فرع معين حسب إحتياجاتك. هيا بنا نبدأ.

## المتطلبات الاساسية

-   معرفة سطحية بطرفية اللاوامر terminal
-   القدرة على كتابة أوامر فى طرفية الاوامر terminal
-   برنامج git منصب على جهازك  (سوف اريك كيفية ذلك)
-   حساب علي Github
-  أبتسامة مشرقة على وجهك (ابتسم يا صديقي)

## مقدمة سريعة عن Git و Github

حسب  [ويكيبيديا](https://en.wikipedia.org/wiki/Git),

> **Git**  iهو نظام موزع للتحكم فى  الاصدارات تم تصميمه لتتبع التغييرات فى مشروع (الكود) فى مجال صناعة البرمجيات. الغرض منه تمكين التنسيق و التعاون و السرعة و الفاعلية بين المطورين..

**GitHub,**  على نحو مختلف ، هو خدمة استضافة علي الانترنت لنظام التحكم في الاصدارات Git. و يقدم لنا كل وظائف ادارة  التحكم فى الاصدارات الموزعة و  الكود  المتاحة من خلال Git و يضيف ايضا الكثير من المزايا لكود الحاسب الالي.

## كيفية تركيب Git  على الويندوز

قم بتنزيل و تركيب اخر اصدار من  [اداة تركيب git على الويندوز  من هنا.](https://git-for-windows.github.io/)

## كيفية تركيب Git على اللينكس

هنا تجد الاوامر حسب توزيعة لينكس الخاصة بيك:

### Debian or Ubuntu

```
sudo apt-get update
sudo apt-get install git
```

### Fedora

```
sudo dnf install git
```

### CentOS

```
sudo yum install git
```

### Arch Linux

```
sudo pacman -Sy git
```

### Gentoo

```
sudo emerge --ask --verbose dev-vcs/git
```

## كيفية تركيب Git على Mac

قم بتنزيل و تركيب اخر اصدار من  [ اداة تركيب git على الماك  من هنا](https://sourceforge.net/projects/git-osx-installer/files/).

أو بإمكانك كتابة هذا الامر:

```
brew install git
```

الان لقد قمنا بتركيب Git ، هيا نكمل هذه المقال.

## مقدمة عن إستنساخ الGit

Git يسمح لك بإدارة و عمل اصدارات من مشروعك/مشاريعك في “المستودع ” (repository ). هذا المستودع (repository) يتم تخزينه على خدمة على الانترنت لأستضافة خدمة التحكم فى الاصدار. مثل  Github.

بأمكانك حينها إستنساخ المستودع (repository) لجهازك المحلي و الحصول على كل الملفات و الفروع (branches) محليا على جهازك.(سوف أشرح  المزيد عن الفروع ).

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-23-at-5.47.48-AM.png)

على سبيل المثال ، بإمكانك إستنساخ freecodecamp’s repository عن طريق مفتاح ssh كما موضح هنا:  

```
git clone git@github.com:freeCodeCamp/freeCodeCamp.git
```

## مقدمة عن فروع جت (Git Branches)

When working on a project, you will likely have different features. And multiple contributors will be working on this project and its features.

Branches allow you to create a "playground" with the same files in the  `master`  branch. You can use this branch to build independent features, test new features, make breaking changes, create fixes, write docs or try out ideas without breaking or affecting the production code. When you're done, you merge the branch into the production  `master`  branch.

Branching is a core concept in Git which is also used in GitHub to manage workflows of different versions of one project. The  `master`  branch is always the default branch in a repository that is most often considered "production and deployable code". New branches like  `passwordless-auth`  or  `refactor-signup-ux`  can be created from the  `master`  branch.

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-22-at-2.47.53-AM.png)

All branches in freeCodeCamp's repository

## How to Clone Git Branches

While you can clone repositories with the  `git clone`  command, keep in mind that this clones the branch and the remote  `HEAD`. This is usually  `master`  by default and includes all other branches in the repository.

So when you clone a repository, you clone the  `master`  and all other branches. This means you will have to checkout another branch yourself.

Let's say your task on a project is to work on a feature to add passwordless authentication to a user dashboard. And this feature is in the  `passwordless-auth`  branch.

You really don't need the  `master`  branch since your "feature branch" will be merged into  `master`  afterward. How then do you clone this  `passwordless-auth`  branch without fetching all other branches with "a bunch of files you don't need"?

I created this sample repository to explain this. This repository holds a simple blog built with Nextjs and has four dummy branches:

-   master
-   dev
-   staging
-   passwordless-auth

In Nextjs, any file inside the folder  `pages/api`  is mapped to the  `/api/*`  path and will be treated as an API endpoint instead of a  `page`. In our repository, I have created different dummy APIs  [in this directory](https://github.com/BolajiAyodeji/nextjs-blog/tree/master/pages/api)  to make each branch different.

The  `master`  branch holds the file  **pages/api/hello.js**  while  `passwordless-auth`  holds the file  **pages/api/auth.js**. Each file just returns a dummy text response. See  `master`'s hello API response  [here](https://nextjs-blog.bolajiayodeji.vercel.app/api/hello)  (with a special message for you ?).

Let's clone the repository:

```
git clone git@github.com:BolajiAyodeji/nextjs-blog.git
```

This gives us access to all branches in this repository and you can easily toggle between each to see each version and its files.

```
git branch -a
```

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-22-at-4.51.56-AM.png)

Wondering where the  **remotes/origin/..**  branches came from?  
  
When you clone a repository, you pull data from a repository on the internet or an internal server known as the  **remote**. The word origin is an alias created by your Git to replace the remote URL (you can change or specify another alias if you want).

These  **remotes/origin/..** branches point you back to the origin repository you cloned from the internet so you can still perform pull/push from the origin.

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-23-at-5.24.43-AM.png)

  
So when you clone  `master`  onto your machine,  `remotes/origin/master`  is the original  `master`  branch on the internet, and  `master`  is on your local machine. So you will pull/push from and to the  `remotes/origin/master`.  
  
In summary  **Remote**  is the URL that points you to the repository on the internet while  **Origin**  is an alias for this remote URL.

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-23-at-5.28.06-AM.png)

## How to Clone a Specific Branch

Now let's clone a specific branch from our demo repository. There are two ways to clone a specific branch. You can either:

-   Clone the repository, fetch all branches, and checkout to a specific branch immediately.
-   Clone the repository and fetch only a single branch.

### Option One

```
git clone --branch <branchname> <remote-repo-url>
```

or

```
git clone -b <branchname> <remote-repo-url>
```

Here  **-b**  is just an alias for  **--branch**

  
With this, you fetch all the branches in the repository, checkout to the one you specified, and the specific branch becomes the configured local branch for  `git push`  and  `git pull`  . But you still fetched all files from each branch. This might not be what you want right? ?

Let's test it:

```
 git clone -b passwordless-auth git@github.com:BolajiAyodeji/nextjs-blog.git
```

This automatically configures  `passwordless-auth`  as the local branch but still tracks other branches.

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-23-at-5.30.01-AM.png)

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-30-at-3.27.31-AM.png)

### Option Two

```
git clone --branch <branchname> --single-branch <remote-repo-url>
```

or

```
git clone -b <branchname> --single-branch <remote-repo-url>
```

Here  **-b**  is just an alias for  **--branch**

This performs the same action as option one, except that the  `--single-branch`  option was introduced in Git version 1.7.10 and later. It allows you to only fetch files from the specified branch without fetching other branches.

Let's test it:

```
git clone -b passwordless-auth --single-branch git@github.com:BolajiAyodeji/nextjs-blog.git
```

This automatically configures  `passwordless-auth`  as the local branch and only tracks this branch.

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-23-at-5.31.12-AM.png)

![](https://www.freecodecamp.org/news/content/images/2020/06/Screenshot-2020-06-30-at-3.29.07-AM.png)

If you run  `cd pages/api`  you'll find the  `auth.js`  file in the  `passwordless-auth`  branch as expected from the previous setup.

## Conclusion

You might be running out of internet or storage space but you need to work on a task in a specific branch. Or you might want to clone a specific branch with limited files for various reasons. Fortunately, Git provides you the flexibility to do this. Flex your muscles and try it out, there's much more "Git" to learn.  
  
One at a time, yeah? ✌?
